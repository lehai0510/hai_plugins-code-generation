<?php

namespace Bss\Funny\Controller\Index;

use Bss\Funny\Helper\Data;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    public function execute()
    {
       $this->_view->loadLayout();
       $this->_view->renderLayout();
    }
}

var config = {

    map: {
        '*': {
            'Magento_Catalog/js/catalog-add-to-cart': 'Bss_Funny/js/catalog-add-to-cart'
        }
    },
    paths: {
        'owlcarousel': "Bss_Funny/js/owl.carousel",
        'widgetowl': "Bss_Funny/js/popup"
    },
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        },
        'widgetowl': {
            deps: ['jquery']
        }
    },
    config: {
        mixins: {
            'Magento_Catalog/js/catalog-add-to-cart': {
                'Bss_Funny/js/catalog-add-to-cart': true
            }
        }
    }
};

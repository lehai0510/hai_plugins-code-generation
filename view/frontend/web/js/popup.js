define(
    [
        'jquery',
        'Bss_Funny/js/popup'
    ],
    function ($) {
        "use strict";
        //creating jquery widget
        $.widget('bss.Popup', {
            _create: function () {
                console.log(this.element)
                console.log(this.options)
                $(this.element).owlCarousel(this.options);
            }
        });
        return $.bss.Popup;
    }
);

<?php

namespace Bss\Funny\Model;

use Bss\Funny\Api\Data;
use Bss\Funny\Api\FunnyRepositoryInterface;
use Bss\Funny\Api\Data\FunnySearchResultsInterface;
use Bss\Funny\Model\ResourceModel\Funny\CollectionFactory;
use Bss\Funny\Model\ResourceModel\Funny as FunnyResource;
use Magento\Framework\Exception\NoSuchEntityException;

class FunnyRepository implements FunnyRepositoryInterface
{
    protected $funnyResource;

    protected $funnyFactory;

    protected $collectionFactory;

    protected $searchResultsFactory;

    public function __construct(
        FunnyResource $funnyResource,
        \Bss\Funny\Model\FunnyFactory $funnyFactory,
        CollectionFactory $collectionFactory,
        FunnySearchResultsInterface $searchResultsFactory
    )
    {
        $this->funnyResource = $funnyResource;
        $this->funnyFactory = $funnyFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    public function save(Data\FunnyInterface $funnyInterface)
    {
        $this->funnyResource->save($funnyInterface);
        return $funnyInterface->getFunnyId();
    }


    public function getById($funnyId)
    {
        $funny = $this->funnyFactory->create();
        $this->funnyResource->load($funny, $funnyId);
        if (!$funny->getId()) {
            throw new NoSuchEntityException('Funny does not exist');
        }
        return $funny;
    }


    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        /** @var Magento\Framework\Api\SortOrder $sortOrder */
        foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
            $field = $sortOrder->getField();
            $collection->addOrder(
                $field,
                $this->getDirection($sortOrder->getDirection())
            );
        }

        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->load();
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setCriteria($searchCriteria);

        $customs = [];
        foreach ($collection as $item) {
            $customs[] = $item;
        }
        $searchResults->setItems($customs);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }


    public function delete($funnyId)
    {
        $funny = $this->funnyFactory->create();
        $funny->setId($funnyId);
        if ($this->funnyResource->delete($funny)) {
            return true;
        } else {
            return false;
        }
    }
}

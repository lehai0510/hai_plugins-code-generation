<?php

namespace Bss\Funny\Model;

use Bss\Funny\Api\Data\FunnyInterface;


class Funny extends \Magento\Framework\Model\AbstractModel implements FunnyInterface
{
    protected $_idFieldName = 'id';

    protected $_eventPrefix = "bss_funny";

    protected function _construct()
    {
        $this->_init('Bss\Funny\Model\ResourceModel\Funny');
    }

    /**
     * @return mixed
     */
    public function getFunnyId()
    {
        return $this->getData(self::FUNNY_ID);
    }

    /**
     * @param $id
     * @return Funny|mixed
     */
    public function setFunnyId($id)
    {
        return $this->setData(self::FUNNY_ID, $id);
    }

    /**
     * @return mixed
     */
    public function getFunny()
    {
        return $this->getData(self::FUNNY);
    }

    /**
     * @param $funny
     * @return Funny|mixed
     */
    public function setFunny($funny)
    {
        return $this->setData(self::FUNNY, $funny);
    }
}

<?php

/**
 * THIS IS A SERVICE CONTRACT
 */

namespace Bss\Funny\Api;

/**
 * @api
 * Interface CustomRepositoryInterface
 * @package Namespace\Custom\api
 */
interface FunnyRepositoryInterface
{
    /**
     * @param Data\FunnyInterface $funnyInterface
     * @return mixed
     */
    public function save(\Bss\Funny\Api\Data\FunnyInterface $funnyInterface);

    /**
     * @param $funnyId
     * @return mixed
     */
    public function getById($funnyId);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * @param $funnyId
     * @return mixed
     */
    public function delete($funnyId);

}

<?php

/**
 *
 */

namespace Bss\Funny\Api\Data;


use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface CustomSearchResultsInterface
 * @package Namespace\Custom\Api\Data
 * @api
 */
interface FunnySearchResultsInterface extends SearchResultsInterface
{

    /**
     * @return \Magento\Framework\Api\ExtensibleDataInterface[]
     */
    public function getItems();

    /**
     * @param array $items
     * @return SearchResultsInterface
     */
    public function setItems(array $items);

}

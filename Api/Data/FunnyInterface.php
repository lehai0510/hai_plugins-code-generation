<?php

/**
 * THIS IS THE DATA CONTAINER
 *
 */

namespace Bss\Funny\Api\Data;


interface FunnyInterface
{
    const FUNNY_ID = 'id';
    const FUNNY = 'funny';
    /**
     * @return mixed
     */
    public function getFunnyId();

    /**
     * @param $id
     * @return mixed
     */
    public function setFunnyId($id);

    /**
     * @return mixed
     */
    public function getFunny();

    /**
     * @param $funny
     * @return mixed
     */
    public function setFunny($funny);
}

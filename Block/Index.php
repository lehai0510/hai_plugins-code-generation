<?php

namespace Bss\Funny\Block;

use Bss\Funny\Api\FunnyRepositoryInterface;
use Bss\Funny\Model\FunnyFactory;
use Magento\Framework\View\Element\Template;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $funnyFactory;

    public function __construct(
        Template\Context $context,
        FunnyFactory $funnyFactory
    )
    {
        $this->funnyFactory = $funnyFactory;
        parent::__construct($context);
    }

    public function sayHello()
    {
        return 'Hello';
    }

    public function getListFunny()
    {
        $funny = $this->funnyFactory->create();
        return $funny->getCollection();

    }
}

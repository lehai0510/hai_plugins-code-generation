<?php

namespace Bss\Funny\Plugin;

use Bss\Funny\Helper\Data;
use Magento\Framework\Message\Manager;
use Magento\Framework\Message\ManagerInterface;

class Message
{
    protected $_funnyData;
    protected $_message;

    /**
     * Message constructor.
     * @param Data $funnyData
     * @param ManagerInterface $message
     */
    public function __construct(
        Data $funnyData,
        ManagerInterface $message
    ) {
        $this->_funnyData = $funnyData;
        $this->_message = $message;
    }

    /**
     * @param Manager $manager
     * @param $result
     * @param $message
     */
    public function beforeAddSuccessMessage(Manager $manager, $message)
    {
        $config = $this->_funnyData->getGeneralConfig('display_text');
        $message = $config . $message;
        return $message;
    }
}
